<?php

use yii\db\Migration;

class m170515_142404_add_age_to_students_table extends Migration
{
    public function up()
    {
		$this->addColumn('student', 'age', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('student','age');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
